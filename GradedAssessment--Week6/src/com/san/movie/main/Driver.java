package com.san.movie.main;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale.Category;
import java.util.Scanner;

import com.san.pojo.factory.Classification;
import com.san.pojo.factory.MovieSearch;
import com.san.singleton.Movie;

public class Driver {

	public static void main(String[] args) {
		int option;

		Scanner scan = new Scanner(System.in);
		do {
			System.out.println("MovieOnTips");
			System.out.println("Please select movie to you want to search");
			System.out.println("1 Movies In Theaters\n" + "2 Upcoming movies\n" + "3 Top Rated Indian\n"
					+ "4 Top Rated Movies\n" + "5 to exit");
			System.out.println("Enter option");
			System.out.println();

			option = scan.nextInt();

			switch (option) {
			case 1:
				Classification show = MovieSearch.setMovieType(2);
				try {
					List<Movie> movie = show.movieType();
					System.out.println("++++++Movies in Theatres++++++");
					for (Movie m : movie) {
						System.out.println(m + "\n");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				Classification upcoming = MovieSearch.setMovieType(1);
				try {
					List<Movie> movie = upcoming.movieType();
					System.out.println("++++++Upcoming Movies++++++");
					for (Movie upc : movie) {
						System.out.println(upc + "\n");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				Classification indian = MovieSearch.setMovieType(3);
				try {
					List<Movie> movie = indian.movieType();
					System.out.println("++++++Top Rated Indian++++++");
					for (Movie india : movie) {
						System.out.println(india + "\n");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			case 4:
				Classification top = MovieSearch.setMovieType(4);
				try {
					List<Movie> movie = top.movieType();
					System.out.println("++++++Top Rated Movies++++++");
					for (Movie high : movie) {
						System.out.println(high + "\n");
					}
				} catch (SQLException e) {
					System.err.println("Exception throws");
					e.printStackTrace();
				}
				break;

			case 5:
				System.exit(0);
				break;
			default:
				System.out.println("Invalid");
				break;
			}
		} while (option != 5);
	}


}
