package com.san.singleton;
public class Movie {

	private int id;
	private String title;
	private int year;
	private int Rating;
	private String classification;

	public Movie() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getRating() {
		return Rating;
	}

	public void setRating(int Rating) {
		this.Rating = Rating;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	@Override
	public String toString() {
		return "movie [id=" + id + ", title=" + title + ", year=" + year + " Rating="+ Rating + ", classification=" + classification + "]";
	}

}
