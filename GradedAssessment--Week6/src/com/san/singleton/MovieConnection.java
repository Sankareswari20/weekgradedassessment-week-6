package com.san.singleton;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MovieConnection {
	private static volatile MovieConnection value;
	private static volatile Connection connection;

	private MovieConnection() {

	}

	public static MovieConnection getInstance() {
		if (value == null) {
			value = new MovieConnection();
		}
		return value;
	}

	public Connection getConnection() {
		
		if (connection == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.main");
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3000/movie", "root", "main");
				System.out.println("driver loaded");
			}
			catch (ClassNotFoundException e) {
				System.err.println("driver not loaded");
				e.printStackTrace();
			}
			catch (SQLException e) {
				System.err.println("Connection interrupted");
				e.printStackTrace();
			}
		}
		return connection;
	}

}



