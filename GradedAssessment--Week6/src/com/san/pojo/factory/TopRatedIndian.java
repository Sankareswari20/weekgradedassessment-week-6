package com.san.pojo.factory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.san.singleton.Movie;
import com.san.singleton.MovieConnection;

public class TopRatedIndian implements Classification {

	Connection conn = MovieConnection.getInstance().getConnection(); // Establishing connection

	public TopRatedIndian() {

	}

	@Override
	public List<Movie> movieType() throws SQLException {

		List<Movie> moviesList = new ArrayList<Movie>(); // displaying movies from database

		// To get data
		String data = "Search top rated indian movie";
		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(data);
		while (result.next()) {
			Movie movie = new Movie();
			movie.setId(result.getInt(1));
			movie.setTitle(result.getString(2));
			movie.setYear(result.getInt(3));
			movie.setRating(result.getInt(5));
			movie.setClassification(result.getString(6));
			moviesList.add(movie);
		}

		return moviesList;
	}
}
